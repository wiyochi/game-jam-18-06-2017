# Game Jam 18/06/2017
Game Jam entre amis, pour le fun.
Thème : "Rythme"
Codé en C++ avec [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library").

---
Arquillière Mathieu - Bastien Goumy - Guillaume Rodrigues - Juin 2017